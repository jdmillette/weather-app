FROM python:3.7-alpine3.11
LABEL author="Justin Millette"
COPY * /weather-app/
RUN pip3 install --user -r /weather-app/requirements.txt
# HEALTHCHECK CMD curl --fail http://localhost:8081/ || exit 1
CMD python /weather-app/weather.py

